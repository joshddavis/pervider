// next.config.js
const withCSS = require('@zeit/next-css')
const withPlugins = require('next-compose-plugins')
const optimizedImages = require('next-optimized-images')


// module.exports = withCSS({
//   /* config options here */
// })

module.exports = withPlugins([
  [optimizedImages, {
    /* config for next-optimized-images */
  }],
  [withCSS, {

  }]
  // your other plugins here
]);