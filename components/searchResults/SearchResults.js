import React from 'react'
import Address from '../address/Address'

const SearchResults = (props) => {

  console.log(props);
  if (props.results.message) {
    return (
      <>
        <h2>{props.results.message}</h2>
      </>
    )
  }

  //Building out google search links
  const searchName = `http://www.google.com/search?q=${props.results.firstName}+${props.results.lastName}`;
  const searchNameCred = `http://www.google.com/search?q=${props.results.firstName}+${props.results.lastName}+${props.results.credentials }`;
  const searchNameAddress = `http://www.google.com/search?q=${props.results.firstName}+${props.results.lastName}+${props.results.addresses[0].street}+${props.results.addresses[0].city }+${props.results.addresses[0].state}`

  //Generating address components
  const primaryAddresses = props.results.addresses.map( x => (<li className="list-group-item" key={x.type}><Address address={x} /></li>));
  const secondaryAddresses = (props.results.secondaryAddresses.length === 0) ? '' :
    props.results.secondaryAddresses.map( x => (<li className="list-group-item" key={x.street}><Address address={x} /></li>));

  return (
  <>
    <h1>Search Results</h1>
    {console.log("Hellos from the component!",props)}
    <div className="card" style={{width: '100%'}}>
      <div className="card-body">
        <h5 className="card-title"><strong>{ props.results.prefix } { props.results.firstName } { props.results.lastName } { props.results.credentials }</strong></h5>
        <h6 className="card-subtitle mb-2 text-muted">Last Updated: {props.results.lastUpdated} </h6>
        <p className="card-text"><strong>Primary Specialty: </strong>{props.results.primarySpecialty}</p>
        <ul className="list-group list-group-flush">
          {primaryAddresses}
          {secondaryAddresses}
        </ul>
        <p className="card-text"><strong>Google Searches:</strong></p>
        <a href={searchNameAddress} className="btn btn-primary" target="_blank">Name & Primary Address</a>
        {(props.results.credentials  === undefined) ? '' : <a href={searchNameCred} className="btn btn-primary" target="_blank">Name & Credential</a>}
        <a href={searchName} className="btn btn-primary" target="_blank">Name Only</a>
      </div>
    </div>
  </>
  )
  }

export default SearchResults;