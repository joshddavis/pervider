import React from 'react'
import Head from 'next/head'
import Nav from '../nav/Navigation'
import { Container } from 'reactstrap'

import 'bootstrap/dist/css/bootstrap.min.css';
import './layout.css'

const Layout = ({children}) => {
  return (
    <>
     <Head>
      <title>Home</title>
      <link rel="icon" href="/favicon.ico" />
     </Head>
     <Nav />
    <main>
      <Container>
        {children}
      </Container>
    </main>
   </>
  );
}

export default Layout;