import React from 'react'

const Address = ({address}) => {

  const formattedZip = (address.zip.length === 5)
                      ? address.zip
                      : `${address.zip.substring(0 ,5)}-${address.zip.substring(5 ,address.zip.length)}`

  return (
    <>
      <p className="card-text"><strong>{address.type}:</strong></p>
      <p className="card-text">{ address.street }</p>
      <p className="card-text">{ address.city }, { address.state } {formattedZip}</p>
      <p className="card-text">{address.country}</p>
      <p className="card-text labl"><strong>Phone / Fax:</strong></p>
      <p className="card-text">Ph: {address.phone} | Fa: {address.fax}</p>
    </>
  )
}

export default Address;


