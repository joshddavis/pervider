
import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { Button, Form, FormGroup, Input } from 'reactstrap';

const SubmitForm = (props) => {

  const [pervider, setPervider] = useState('');
  const router = useRouter();

  const submitHandler = async (event) => {
    console.log("Handler fired!!! State is currently: ", pervider["0"].npi);
    const query = pervider["0"].npi;
    event.preventDefault();
    router.push(`/search-results?npi=${query}`)
  }

  const changeHandler = (event) => {
    setPervider([
      {
        npi: event.target.value
      }
    ]);
  }

  return (
    <Form onSubmit={submitHandler}>
      <FormGroup>
        <Input type="number" name="npi" id="enterNPI" placeholder="Enter NPI number" onChange={changeHandler} />
        <Button color="primary">Submit</Button>
      </FormGroup>
    </Form>
  )
}

export default SubmitForm;