import React from 'react'

import Layout from '../components/layouts/Layout'

function Error({ statusCode }) {

    let heading;
    let message;

    if (statusCode) {
        if (statusCode === 404) {
            heading = 'Page Not Found'
            message = 'Whoops! The page you were looking for does not exist. Please use the naviagtion menu to get where you need to go.'
        }
        else {
            heading = 'Error'
            message = `${statusCode} error encountered on the server.`
        }
    } else {
        heading = 'Client-Side Error';
        message = 'An error occured on the client. Sorry!'
    }
  return (
      <Layout>
        <h1>{heading}</h1>
        <p>{message}</p>
      </Layout>
  )
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

export default Error