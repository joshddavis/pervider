import React from 'react'

import Layout from '../components/layouts/Layout'
import SubmitForm from '../components/submitForm/SubmitForm'
import { Jumbotron } from 'reactstrap';

const Home = () => (
  <Layout>
   <Jumbotron>
      <h1 className="display-3">PERVIDER</h1>
      <p className="lead">A clean and simple NPI lookup service with integrated Google search.</p>
    </Jumbotron>
    <SubmitForm />
  </Layout>
  
)

export default Home
