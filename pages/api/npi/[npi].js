import fetch from 'isomorphic-unfetch'

export default async (req, res) => {
  const {
    query: { npi },
  } = req

  // If no NPI somehow ends up being sent
  if (npi === 'undefined') {
    let error = {
      message: 'No NPI sent for search. Please search again.'
    }
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 200;
    res.end(JSON.stringify(error));
  } else {

  const regres = await fetch(`https://npiregistry.cms.hhs.gov/api/?version=2.0&number=${npi}`);
  const json = await regres.json();

  //If no results found, return message
  if (json.result_count === 0) {
    let error = {
      message: 'No providers found. Please try again.'
    }
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 200;
    res.end(JSON.stringify(error));
  }

  console.log('API: Registry Object: ', JSON.stringify(json, null, 2))

  //Build secondary addresses if they exist
  let secondaryAddresses = [];
  if (json.results[0].practiceLocations) {
    secondaryAddresses = json.results[0].practiceLocations.map(x => ({
      type: 'Secondary Address',
      street: x.address_1,
      city: x.city,
      state: x.state,
      zip: x.postal_code,
      country: x.country_name,
      phone: x.telephone_number,
      fax: x.fax_number
    }))
  }

  //Build the returned provider object
  const pervider = {
    lastUpdated: json.results[0].basic.last_updated,
    prefix: json.results[0].basic.name_prefix,
    firstName: json.results[0].basic.first_name,
    lastName: json.results[0].basic.last_name,
    credentials: json.results[0].basic.credential,
    primarySpecialty: json.results[0].taxonomies[0].desc,
    addresses: [
      {
        type: 'Primary Practice Address',
        street: json.results[0].addresses[0].address_1,
        city: json.results[0].addresses[0].city,
        state: json.results[0].addresses[0].state,
        zip: json.results[0].addresses[0].postal_code,
        country: json.results[0].addresses[0].country_name,
        phone: json.results[0].addresses[0].telephone_number,
        fax: json.results[0].addresses[0].fax_number
      },
      {
        type: 'Mailing Address',
        street: json.results[0].addresses[1].address_1,
        city: json.results[0].addresses[1].city,
        state: json.results[0].addresses[1].state,
        zip: json.results[0].addresses[1].postal_code,
        country: json.results[0].addresses[1].country_name,
        phone: json.results[0].addresses[1].telephone_number,
        fax: json.results[0].addresses[1].fax_number
      }
    ],
    secondaryAddresses,
  }

  console.log('Pervider: ', JSON.stringify(pervider, null, 2));

  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.end(JSON.stringify(pervider));
  }

}



