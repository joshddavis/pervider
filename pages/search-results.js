import React from 'react'
import Layout from '../components/layouts/Layout'
import fetch from 'isomorphic-unfetch'
import apiUrl from '../lib/helpers/apiUrl';
import SearchResults from '../components/searchResults/SearchResults'
import SubmitForm from '../components/submitForm/SubmitForm';


const SearchResultsPage = ({ json }) => (
  <Layout>
    <SearchResults results={ json } />
    <h1>Search Again</h1>
    <SubmitForm />
  </Layout>
)

SearchResultsPage.getInitialProps = async ({ req, query }) => {
  const url = apiUrl(`/api/npi/${query.npi}`, req);
  const res = await fetch(url);
  const json = await res.json();
  return { json }
}

export default SearchResultsPage
