import React from 'react'

import Layout from '../components/layouts/Layout'

const AboutPage = () => {
  return(
    <Layout>
      <div className="container-fluid">
        <h1>About</h1>
        <div className="card mb-3" style={{maxWidth: '1140px'}}>
          <div className="row no-gutters">
            <div className="col-md-3">
              <img src={require('../img/me.jpg')} className="card-img" alt="me" />
            </div>
            <div className="col-md-9">
              <div className="card-body">
                <h5 className="card-title"><strong>Hi I'm Josh!</strong></h5>
                  <p className="card-text">I created PERVIDER as a learning exercise to see how quickly I could spin up a useful application using <a href="https://nextjs.org/">NextJS</a>.</p>
                  <p className="card-text">At my current job, I spend a lot of time looking up health-care provider details via their NPI number. Usually, the next step is to open a seperate browser tab and perform a Google search on the name or address that gets returned from the NPI registry. This tool saves me several clicks versus using the government's <a href="https://npiregistry.cms.hhs.gov/">NPI registry website</a>. For example, there's no need to return a results page if you're searching by just the NPI number. Either return the provider if one is found, or don't.</p>
                  <p className="card-text">
                  I plan to add a name search and results page in the future, but for now I hope you find this simple tool as useful as I do. Hit me up on twitter <a href="https://twitter.com/SmoshDavis">@SmoshDavis</a> if you want to suggest a feature, say hi, or just tell me I suck. Cheers!
                  </p>
                  <p className="card-text"><small className="text-muted">December, 2019</small></p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </Layout>
  )
}

export default AboutPage